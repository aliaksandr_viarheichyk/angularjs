let app =  angular.module('app', []);

app.factory('Todos', () => {
    return [
        {
            "title": "React",
            "description": "Learn React",
            "isDone": "inProcess"
        }, {
            "title": "AngularJS",
            "description": "Learn AngularJS",
            "isDone": "inProcess"
        }, {
            "title": "Rewrite code",
            "description": "Rewrite old code",
            "isDone": false
        }, {
            "title": "New app",
            "description": "Create new app",
            "isDone": true
        }, {
            "title": "Sleep",
            "description": "I want to sleep",
            "isDone": false
        }
    ]
});

app.controller('homeController', ($scope, Todos) => {
    $scope.items = Todos;
    $scope.isShowAddForm = false;
    $scope.item = {};

    $scope.getItemClass = (isDone) => {
        if(isDone === 'inProcess')
            return { 'todoList__item_inProcess': true };
        else if(isDone)
            return { 'todoList__item_done': true };
    }

    $scope.isDoneFilter = (value) => {
        $scope.status = { isDone: value };
    }

    $scope.setIsDone =(item, value) => {
        item.isDone = value;
    }

    $scope.showAddForm = () => {
        $scope.isShowAddForm = !$scope.isShowAddForm;
    }

    $scope.submit = function() {
        Todos.push($scope.item);
        $scope.item.isDone = false;
        $scope.item = {};
    }
});